# AsClaras.org Extractor

AsClaras.org is an website that gather a lot of information about brazilian politician candidates and parties.

Obs: this isn't a SDK, but a application that can crawl asclaras website. You can integrate it with your application by importing the generated jar file, or simple doing the steps above to get the structured data.

This project has the objective to extract information from brazilian site asclaras.org into csv files, that contains informations about brazilian politicians campains including vote costs, donations and others.

Esse projeto tem como objetivo extrair informações do site brasileiro asclaras.org em arquivos csv, contendo informações sobre políticos brasileiros, incluindo custo de voto e doações.
<hr>

The parameters that you can pass to the program is:

Os parâmetros que podem ser passados para o programa são:

<i>output:\<output_csv_path\> numofPages:\<num_of_pages_to_fetch\> offset:\<offset\> ano:\<year\> cargo:\<role\> partido:\<party\> estado:/<state\> municipio:\<city\> eleito:\<elect\></i>

Only output and numOfPages are required, the other arguments are opcional.
Apeás output e numOfPages são obrigatórios.

<br>
<br>

Feel free to collaborate to this project!
